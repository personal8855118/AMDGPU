#!/bin/bash
# Polkit authentication and executing the script with administrative privileges
/usr/bin/pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY bash <<'EOF'

card_base="/sys/class/drm/card1/device"
echo "manual" > $card_base/power_dpm_force_performance_level
echo "s 0 300 750" > $card_base/pp_od_clk_voltage 
echo "s 1 909 850" > $card_base/pp_od_clk_voltage 
echo "s 2 1169 1068" > $card_base/pp_od_clk_voltage 
# echo "s 3 1301 1160" > $card_base/pp_od_clk_voltage 
# echo "s 4 1400 1160" > $card_base/pp_od_clk_voltage 
# echo "s 5 1467 1160" > $card_base/pp_od_clk_voltage 
# echo "s 6 1525 1160" > $card_base/pp_od_clk_voltage 
# echo "s 7 1550 1160" > $card_base/pp_od_clk_voltage 
echo "s 3 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 4 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 5 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 6 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 7 1450 1100" > $card_base/pp_od_clk_voltage 
echo "m 0 400 750" > $card_base/pp_od_clk_voltage 
echo "m 1 1000 900" > $card_base/pp_od_clk_voltage 
echo "m 2 1800 940" > $card_base/pp_od_clk_voltage 
echo "c" > $card_base/pp_od_clk_voltage

pushd $card_base
echo 0 1 2 3 4 5 6 | tee pp_dpm_sclk
echo 0 1 2 | tee pp_dpm_mclk
popd

EOF

# AMDGPU

This repository contains scripts for optimizing and configuring AMD Radeon GPUs on Fedora Linux. Please note that using these scripts carries a risk, and the author of this repository is not responsible for any issues or damages that may occur to your computer or operating system. You are solely responsible for any consequences that arise from the use of these scripts on your system.

You don't need to run the script via terminal. Instead, open the directory where the "Properties" script is located and right-click on the file. Then, select the option "Executable as Program".

## Fedora AMD Radeon GPU All Permission Script 

This script grants all permissions for AMD Radeon GPUs on Fedora Linux.

```console
#!/bin/bash
# Polkit authentication
/usr/bin/pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY grubby --args=amdgpu.ppfeaturemask=0xffffffff --update-kernel=ALL
```

## Fedora AMD Radeon GPU Undervolt Script

This script is specifically for AMD Radeon RX 590, but can be modified to fit other GPUs as well. Please change the number of "card1" to the corresponding number in the "/sys/class/drm/" dictionary.

```console
#!/bin/bash
# Polkit authentication and executing the script with administrative privileges
/usr/bin/pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY bash <<'EOF'

card_base="/sys/class/drm/card1/device"
echo "manual" > $card_base/power_dpm_force_performance_level
echo "s 0 300 750" > $card_base/pp_od_clk_voltage 
echo "s 1 909 850" > $card_base/pp_od_clk_voltage 
echo "s 2 1169 1068" > $card_base/pp_od_clk_voltage 
# echo "s 3 1301 1160" > $card_base/pp_od_clk_voltage 
# echo "s 4 1400 1160" > $card_base/pp_od_clk_voltage 
# echo "s 5 1467 1160" > $card_base/pp_od_clk_voltage 
# echo "s 6 1525 1160" > $card_base/pp_od_clk_voltage 
# echo "s 7 1550 1160" > $card_base/pp_od_clk_voltage 
echo "s 3 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 4 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 5 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 6 1450 1100" > $card_base/pp_od_clk_voltage 
echo "s 7 1450 1100" > $card_base/pp_od_clk_voltage 
echo "m 0 400 750" > $card_base/pp_od_clk_voltage 
echo "m 1 1000 900" > $card_base/pp_od_clk_voltage 
echo "m 2 1800 940" > $card_base/pp_od_clk_voltage 
echo "c" > $card_base/pp_od_clk_voltage

pushd $card_base
echo 0 1 2 3 4 5 6 | tee pp_dpm_sclk
echo 0 1 2 | tee pp_dpm_mclk
popd

EOF
```

## More Information

For more information on configuring and optimizing AMD GPUs on Fedora Linux, please refer to the following resources:

• https://gitlab.freedesktop.org/drm/amd/-/issues/1022

• https://www.reddit.com/r/Amd/comments/agwroj/how_to_overclock_your_amd_gpu_on_linux/

• https://wiki.archlinux.org/title/AMDGPU#Manually

• https://www.reddit.com/r/linux_gaming/comments/vo8hv2/how_to_configure
